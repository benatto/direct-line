# Sum List Service

Create a REST endpoint that return the sum of a list of numbers e.g. `[1,2,3] => 1+2+3 = 6`.
You are free to use any Python 3 framework, however, try and keep the usage of the third- party library to a minimum.  

## Dependencies

The application was build using Python3 and Docker. If you prefer you can use virtual environments

* Python3 (Flask, PyTest)
* Docker (optional)
* Make (optional)

## Build with Docker

To build the project

```
$ make build up

sum-list_1  |  * Serving Flask app "app" (lazy loading)
sum-list_1  |  * Environment: production
sum-list_1  |    WARNING: Do not use the development server in a production environment.
sum-list_1  |    Use a production WSGI server instead.
sum-list_1  |  * Debug mode: on
sum-list_1  |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
sum-list_1  |  * Restarting with stat
sum-list_1  |  * Debugger is active!
sum-list_1  |  * Debugger PIN: 234-199-036
```

# Build with Virtual Env

```
$ python3 -m venv venv
$ pip3 install -r requirements.txt
$ python3 app.py
```

## To run tests

```
$ make test

----------- coverage: platform linux, python 3.8.5-final-0 -----------
Name                       Stmts   Miss  Cover   Missing
--------------------------------------------------------
app.py                        16      1    94%   26
project/resources/sum.py      16      3    81%   11, 20-21
routes.py                      3      0   100%
tests/test_sum.py             20      0   100%
--------------------------------------------------------
TOTAL                         55      4    93%
```

## Example Numbers for POST

The endpoint: `http://localhost:5000/total`

```
{
    "numbers": [1, 2, 3]
}
```

Response:

```
{
    "total": 6
}
```

**If you do not provide a body, by default the service will use: `sum(list(range(10000001)))`**

## Considerations

### About the architecture

* This is a coding challenge but I would architect the solution in a different way depending the number of requests:
  1. I would create a pipeline;
  2. The message (with the list of numbers) first would be added to a Queue;
  3. Multiple services can read messages from the Queue;
  4. Post the result of processment in another Queue;
  5. Multiple services can read from the Queue and store or do anything else;
  6. In my humble opinion this solution would scale better;

### About the code

* I like to use `Makefile` to manage the service
* I like to use `mypy` to catch bug in early stage
* I like to use `docker` to help with testing, running local, deploy and stop with the quote: "It runs in my computer :)"
* I used `flask` because it is simple, but depending on the solution we might need some `async` framework
* I use `black` to help me with coding standards
