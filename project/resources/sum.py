import json

from flask_restful import Resource
from flask import request


class SumList(Resource):
    def post(self):
        try:
            if not request.data:
                # README: This is hardcoded because of the exercise description
                return {"total": sum(list(range(10000001)))}, 200

            numbers = json.loads(request.data).get("numbers", None)
            if numbers is None or not isinstance(numbers, list):
                return {"error": f"Numbers are not valid: {numbers}"}, 400

            return {"total": sum(numbers)}, 200
        except TypeError as err:
            return {"error": f"Numbers are not valid: {str(err)}"}, 400
        except Exception as err:
            return {"error": f"Unexpected error: {str(err)}"}, 500
