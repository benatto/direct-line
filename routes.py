from project.resources.sum import SumList


def add(api):
    # Add all endpoints here
    api.add_resource(SumList, "/total")
