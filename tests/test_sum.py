import pytest

from app import app

app.testing = True
app.debug = True


@pytest.mark.parametrize(
    "numbers, total, status",
    [
        ([1, 2, 3], 6, 200),
        ([0, 0, 0], 0, 200),
        ([0, 0, -1], -1, 200),
        ([], 0, 200),
        ((5, 6), 11, 200),
    ],
)
def test_several_valid_entries(numbers, total, status):
    # Act
    response = app.test_client().post("/total", json={"numbers": numbers})

    # Assert
    assert total == response.json["total"]
    assert status == response.status_code


@pytest.mark.parametrize(
    "numbers, status", [("", 400), (1, 400), ({}, 400), ([1, 2, "3"], 400)]
)
def test_several_invalid_entries(numbers, status):
    # Act
    response = app.test_client().post("/total", json={"numbers": numbers})

    # Assert
    assert "total" not in response.json
    assert status == response.status_code


def test_sum_returns_405_for_invalid_method():
    # Act
    response = app.test_client().get("/total")

    # Assert
    assert 405 == response.status_code


def test_sum_returns_404_for_invalid_path():
    # Act
    response = app.test_client().get("/invalid-path")

    # Assert
    assert 404 == response.status_code


# This is use default list when the body does not exist
def test_sum_list_without_body_use_range_list():
    ## Arrange
    numbers_to_add = sum(list(range(10000001)))

    # Act
    response = app.test_client().post("/total")

    # Assert
    assert response.json["total"] == numbers_to_add
